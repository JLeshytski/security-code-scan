FROM golang:1.17-alpine AS build

ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /go/src/buildapp
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
      PATH_TO_MODULE=`go list -m` && \
      go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM alpine:3

# Install mono and .NET, partially based on https://docs.microsoft.com/en-us/dotnet/core/install/linux-alpine
# We have to add the certs below and import them into the mono cert store so
# that we can install dependencies through nuget
# (https://github.com/Docker-Hub-frolvlad/docker-alpine-mono/pull/8/files)
RUN echo "https://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
RUN apk update && \
    apk add --no-cache git bash openssl icu-libs krb5-libs libgcc libintl libssl1.1 libstdc++ zlib wget mono-dev && \
    apk upgrade && \
    apk add --update --no-cache ca-certificates && \
    cert-sync /etc/ssl/certs/ca-certificates.crt && \
    apk del ca-certificates

# Install msbuild and nuget binaries
RUN mkdir -p /root/.dotnet && \
    wget https://github.com/mono/msbuild/releases/download/v16.9.0/mono_msbuild_6.12.0.137.zip && \
    unzip mono_msbuild_6.12.0.137.zip -d /root/.dotnet && \
    rm mono_msbuild_6.12.0.137.zip && \
    (cd /root/.dotnet && wget https://dist.nuget.org/win-x86-commandline/latest/nuget.exe) && \
    echo -e "#!/bin/bash\nmono /root/.dotnet/nuget.exe \"\$@\" -MSBuildPath /root/.dotnet/msbuild/" >> /usr/bin/nuget && \
    chmod +x /usr/bin/nuget && \
    echo -e "#!/bin/bash\nmono /root/.dotnet/msbuild/MSBuild.dll \"\$@\"" >> /usr/bin/msbuild && \
    chmod +x /usr/bin/msbuild

# Install dotnet
RUN wget https://dot.net/v1/dotnet-install.sh && \
    chmod +X dotnet-install.sh && \
    bash ./dotnet-install.sh -c 5.0 && \
    bash ./dotnet-install.sh -c 3.1 && \
    bash ./dotnet-install.sh -c 2.1 && \
    rm dotnet-install.sh

ENV DOTNET_ROOT=/root/.dotnet
ENV PATH=$PATH:$DOTNET_ROOT

# Install security-code-scan
ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-5.2.1}

RUN mkdir /root/security-code-scan && \
      cd /root/security-code-scan/ && \
      wget https://github.com/security-code-scan/security-code-scan/releases/download/$SCANNER_VERSION/SecurityCodeScan.VS2019.Vsix.vsix && \
      unzip SecurityCodeScan.VS2019.Vsix.vsix && \
      rm SecurityCodeScan.VS2019.Vsix.vsix

COPY --from=build /analyzer /analyzer

RUN \
    mkdir /.dotnet; \
    mkdir /.nuget; \
    mkdir /.local; \
    chmod -R g+rw /.dotnet; \
    chmod -R g+rw /.nuget; \
    chmod -R g+rw /.local;

ENTRYPOINT []
CMD ["/analyzer", "run"]
